from typing import List
import typer
from rich.console import Console
import logging
from rich.logging import RichHandler

from awsec.ui.guardduty import app as gd_app
from awsec.adapters.uow import AwsUnitOfWork


console = Console()
app = typer.Typer()

FORMAT = "%(message)s"


@app.callback()
def main(
    ctx: typer.Context,
    verbose: List[bool] = typer.Option(
        False,
        "--verbose",
        "-v",
        help="Output more details on what is happening. Use multiple times for more details.",
    ),
) -> None:
    level = "WARNING"
    if len(verbose) == 1:
        level = "INFO"
    if len(verbose) >= 2:
        level = "DEBUG"

    logging.basicConfig(
        level=level, format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
    )

    ctx.obj = {"uow": AwsUnitOfWork()}


app.add_typer(gd_app, name="guard-duty")


typer_click_object = typer.main.get_command(app)

if __name__ == "__main__":
    app()

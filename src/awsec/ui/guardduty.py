import typer
from rich.console import Console

app = typer.Typer()
console = Console()


@app.command()
def list_findings(
    ctx: typer.Context,
) -> None:
    """
    List all current findings.
    """
    uow = ctx.obj["uow"]
    print(uow)

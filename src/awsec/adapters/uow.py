import abc
from typing import Any
from boto3 import client


class AbstractUnitOfWork(abc.ABC):
    guard_duty_client: Any

    def __enter__(self, *args) -> None:
        pass

    def __exit__(self, *args) -> None:
        self.rollback()

    @abc.abstractmethod
    def commit(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def rollback(self) -> None:
        raise NotImplementedError


class AwsUnitOfWork(AbstractUnitOfWork):
    guard_duty_client = client("guardduty")

    def __enter__(self, *args) -> None:
        pass

    def __exit__(self, *args) -> None:
        self.rollback()

    def commit(self) -> None:
        pass

    def rollback(self) -> None:
        pass

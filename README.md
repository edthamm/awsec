# AwSec

Welcome to AwSec. You can find more extensive documentation over at [readthedocs](https://awsec.readthedocs.io/en/latest/).

This is a small cli tool to make life a little easier for the overworked responder.

Contributions are welcome. Just get in touch.

## Quickstart

Simply `pip install awsec` and get going. The cli is available as `awsec` and
you can run `awsec --help` to get up to speed on what you can do.

## Development

This project uses `poetry` for dependency management and `pre-commit` for local checks.
